import { setGroupStates, fetchGroups } from "./huegroup";
import { setLightStates, fetchLights } from "./huelight";
import {
  setSceneStates,
  fetchScenes,
  fetchScene,
  filterScenesBy
} from "./huescene";
import { HUE_STATES } from "./hue";

export class HueAdapter {
  static HUETYPE_LIGHT = "light";
  static HUETYPE_GROUP = "group";
  static HUETYPE_SCENE = "scene";

  #adapter = {
    light: {
      huestate: "state",
      hueicon: "config.archetype",
      name: "name",
      type: "type",
      setters: {
        huestate: setLightStates
      },
      getters: {
        all: fetchLights
      }
    },
    group: {
      huestate: "action",
      hueicon: "class",
      name: "name",
      type: "type",
      setters: {
        huestate: setGroupStates
      },
      getters: {
        all: fetchGroups
      }
    },
    scene: {
      huestate: "lightstates",
      name: "name",
      setters: {
        huestate: setSceneStates
      },
      getters: {
        all: fetchScenes,
        byId: fetchScene
      }
    }
  };

  constructor(id, huetype, data) {
    this.id = id;
    this.huetype = huetype;
    this.data = data;
  }

  /**
   * Construct a HueAdapter by determining huetype thanks to
   * the data. Currently, the deduction of the huetype is empirical
   *
   * Huetype:
   *  HUETYPE_LIGHT => data has property 'productid'
   *  HUETYPE_GROUP => data has property 'action'
   *  HUETYPE_SCENE => data has property 'picture'
   *
   * @param {String} id
   * @param {Object} data
   */
  static byData(id, data) {
    if (Object.prototype.hasOwnProperty.call(data, "productid")) {
      return new this(id, HueAdapter.HUETYPE_LIGHT, data);
    } else if (Object.prototype.hasOwnProperty.call(data, "action")) {
      return new this(id, HueAdapter.HUETYPE_GROUP, data);
    } else if (Object.prototype.hasOwnProperty.call(data, "picture")) {
      return new this(id, HueAdapter.HUETYPE_SCENE, data);
    } else {
      // error
      throw "*****Undefined hue adapter.";
    }
  }

  /**
   * Set hue states according to the huetype
   *
   * @param {Object} data
   */
  async setHueState(data) {
    if (Object.prototype.hasOwnProperty.call(data, HUE_STATES.SAT.field)) {
      data.sat = parseInt(data.sat);
      this.checkSat(data.sat);
    }
    if (Object.prototype.hasOwnProperty.call(data, HUE_STATES.BRI.field)) {
      data.bri = parseInt(data.bri);
      this.checkBri(data.bri);
    }
    Object.assign(this.data, data);
    return this.#adapter[this.huetype].setters.huestate(this.id, data);
  }

  /**
   * Check if saturation does not overflow
   *
   * @param {Int} value
   *
   * @returns {Boolean} true
   */
  checkSat(value) {
    if (value > HUE_STATES.SAT.max || value < HUE_STATES.SAT.min) {
      throw `*****SatError: sat must be between ${HUE_STATES.SAT.min} and ${HUE_STATES.SAT.max}`;
    }
    return true;
  }

  /**
   * Check if brightness does not overflow
   *
   * @param {Int} value
   *
   * @returns {Boolean} true
   */
  checkBri(value) {
    if (value > HUE_STATES.BRI.max || value < HUE_STATES.BRI.min) {
      throw `*****BriError: bri must be between ${HUE_STATES.BRI.min} and ${HUE_STATES.BRI.max}`;
    }
    return true;
  }

  /**
   * Return group's scenes if adapter is group else null
   *
   * @param {Object} scenes
   *
   * @returns {Object} filteredScenes
   */
  extractScenes(scenes) {
    if (this.huetype === HueAdapter.HUETYPE_GROUP) {
      return filterScenesBy(Object.entries(scenes), "group", this.id);
    }
    return null;
  }

  details() {
    switch (this.huetype) {
      case HueAdapter.HUETYPE_LIGHT:
        return {
          name: this.name,
          type: this.type,
          modelid: this.data["modelid"],
          productid: this.data["productid"],
          productname: this.data["productname"]
        };
      case HueAdapter.HUETYPE_GROUP:
        return {
          name: this.name,
          type: this.type,
          class: this.data["class"],
          state: this.data["state"],
          lights: this.data["lights"],
          recycle: this.data["recycle"]
        };
    }
  }

  get name() {
    return this.data[this.#adapter[this.huetype].name];
  }

  get on() {
    return this.data[this.#adapter[this.huetype].huestate].on;
  }

  get bri() {
    return this.data[this.#adapter[this.huetype].huestate].bri;
  }

  get sat() {
    return this.data[this.#adapter[this.huetype].huestate].sat;
  }

  get xy() {
    return this.data[this.#adapter[this.huetype].huestate].xy;
  }

  get ct() {
    return this.data[this.#adapter[this.huetype].huestate].ct;
  }

  get reachable() {
    if (this.huetype === HueAdapter.HUETYPE_LIGHT)
      return this.data[this.#adapter[this.huetype].huestate].reachable;
    return undefined;
  }

  get type() {
    return this.data[this.#adapter[this.huetype].type];
  }

  get icon() {
    let iconProperty = this.#adapter[this.huetype].hueicon.split(".");
    let _ = { ...this.data };
    for (let property of iconProperty) {
      _ = _[property];
    }
    return `hue-icons/${_}.svg`;
  }

  get effect() {
    return this.data[this.#adapter[this.huetype].huestate].effect;
  }

  set on(value) {
    this.data[this.#adapter[this.huetype].huestate].on = value;
  }

  set bri(value) {
    this.checkBri();
    this.data[this.#adapter[this.huetype].huestate].bri = value;
  }

  set sat(value) {
    this.checkSat();
    this.data[this.#adapter[this.huetype].huestate].sat = value;
  }

  set xy(value) {
    this.data[this.#adapter[this.huetype].huestate].xy = value;
  }

  set ct(value) {
    this.data[this.#adapter[this.huetype].huestate].ct = value;
  }

  set effect(value) {
    this.data[this.#adapter[this.huetype].huestate].effect = value;
  }
}
