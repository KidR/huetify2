import { devBridge } from "./huebridge";

const bridge = devBridge();

const API_RESOURCE = "lights";

/**
 * Retrieve all lights
 */
export async function fetchLights() {
  const response = await fetch(bridge.access(`/${API_RESOURCE}/`), {
    method: "GET",
    headers: { "Content-Type": "application/json" }
  });
  return await response.json();
}

/**
 * Set light states
 *
 * @param {String} id
 * @param {Object} states
 */
export async function setLightStates(id, states) {
  const response = await fetch(bridge.access(`/${API_RESOURCE}/${id}/state/`), {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(states)
  });
  return await response.json();
}
