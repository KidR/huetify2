import { xyToRGB, HUE_STATES, briToAlpha } from "@/lib/hue";
import { HUE_GROUP_TYPES } from "@/lib/huegroup";

/**
 * Create a CSS gradient style if arrGradient length is > 1
 * else it returns a single color-background
 *
 * @param {Array} lightstates
 *
 * @returns {String} css-style gradient
 */
export function createGradientCSS(lightstates) {
  if (lightstates.length === 1) {
    return createBackgroundCSS(lightstates.pop());
  }
  let cssGradient = "background: linear-gradient(145deg";
  for (let state of Object.values(lightstates)) {
    if (HUE_STATES.XY.field in state) {
      let rgb = xyToRGB(state.xy[0], state.xy[1], state.bri);
      rgb.a = briToAlpha(state.bri);
      cssGradient += `,${RGBAColorCSS(rgb)}`;
    }
  }
  cssGradient += ");";
  return cssGradient;
}

/**
 * Create a CSS rgb background.
 *
 * @param {Object} rgb
 *
 * @returns {String} css-style background
 */
export const createBackgroundCSS = rgb => "background: " + RGBColorCSS(rgb);

/**
 * Create a CSS color in RGBA format: rgba(r,g,b,a)
 *
 * @param {Object} rgba
 *
 * @returns {String} css-style rgba
 */
export const RGBAColorCSS = rgba =>
  `rgba(${rgba.r},${rgba.g},${rgba.b},${rgba.a})`;

/**
 * Create a CSS color in RGB format: rgb(r,g,b)
 *
 * @param {Object} rgb
 *
 * @returns {String} css-style rgb
 */
export const RGBColorCSS = rgb => `rgb(${rgb.r},${rgb.g},${rgb.b})`;

/**
 * Filter entries by removing undesired term
 *
 * @param {Array} entries
 *
 * @returns {Array} filtered array
 */
export function filterRemoveExcludedEntries(entries, excluded = null) {
  excluded = excluded === null ? [HUE_GROUP_TYPES.ENTERTAINMENT] : excluded;
  return entries.filter(entry => !excluded.includes(entry[1].type));
}

/**
 * Format items with filters and return a sorted Array
 *
 * @param {Object} items
 *
 * @returns {Array} filtered array
 */
export function formattedItems(items, excluded = null) {
  let filtered = Object.entries(items);
  filtered = filterRemoveExcludedEntries(filtered, excluded);
  filtered = filtered.sort((x, y) => x[1].name.localeCompare(y[1].name));
  return filtered.map(e => Object.assign({}, { id: e[0], data: e[1] }));
}
