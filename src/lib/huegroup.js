import { devBridge } from "./huebridge";

const bridge = devBridge();

const API_RESOURCE = "groups";

export const HUE_GROUP_TYPES = {
  ENTERTAINMENT: "Entertainment",
  ROOM: "Room",
  ZONE: "Zone"
};

/**
 * Retrieves all groups
 */
export async function fetchGroups() {
  const response = await fetch(bridge.access(`/${API_RESOURCE}/`), {
    method: "GET",
    headers: { "Content-Type": "application/json" }
  });
  return await response.json();
}

/**
 * Set group state
 *
 * @param {String} id
 * @param {Object} states
 */
export async function setGroupStates(id, states) {
  const response = await fetch(
    bridge.access(`/${API_RESOURCE}/${id}/action/`),
    {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(states)
    }
  );
  return await response.json();
}
