import Vue from "vue";
import Vuetify from "vuetify/lib/framework";
import VueLogger from "vuejs-logger";

const loggerOptions = {
  isEnabled: true,
  logLevel: process.env.NODE_ENV === "production" ? "error" : "debug",
  stringifyArguments: false,
  showLogLevel: true,
  showMethodName: false,
  separator: "::",
  showConsoleColors: true
};

Vue.use(VueLogger, loggerOptions);
Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: "mdi"
  }
});
