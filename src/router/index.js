import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

export const ROUTES = {
  HOME: {
    NAME: "home",
    PATH: "/"
  },
  LIGHTS: {
    NAME: "lights",
    PATH: "/lights"
  },
  GROUPS: {
    NAME: "groups",
    PATH: "/groups"
  },
  EFFECTS: {
    NAME: "effects",
    PATH: "/effects"
  },
  DEVTEST: {
    NAME: "devtest",
    PATH: "/devtest"
  }
};

const routes = [
  {
    path: ROUTES.HOME.PATH,
    name: ROUTES.HOME.NAME,
    component: () => import(/* webpackChunkName: "home" */ "@/views/Home")
  },
  {
    path: ROUTES.LIGHTS.PATH,
    name: ROUTES.LIGHTS.NAME,
    component: () => import(/* webpackChunkName: "lights" */ "@/views/Lights")
  },
  {
    path: ROUTES.GROUPS.PATH,
    name: ROUTES.GROUPS.NAME,
    component: () => import(/* webpackChunkName: "groups" */ "@/views/Groups")
  },
  {
    path: ROUTES.EFFECTS.PATH,
    name: ROUTES.EFFECTS.NAME,
    component: () => import(/* webpackChunkName: "effects" */ "@/views/Effects")
  },
  {
    path: ROUTES.DEVTEST.PATH,
    name: ROUTES.DEVTEST.NAME,
    component: () => import(/* webpackChunkName: "devtest" */ "@/views/Devtest")
  }
];

const router = new VueRouter({
  routes
});

export default router;
