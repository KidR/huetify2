# huetify2

## Dev Install
### Project setup

#### 1. Set Philips Hue API key

1. Go to `src/lib/huebridge.js`
2. Replace with your own developer Hue API credentials

```js
export function devBridge() {
  return new HueBridge(
    "<Hue bridge @IP>",
    "<Hue Developer KEY>"
  );
}
```

#### 2. Launch app in dev mode

```sh
npm install
npm run serve
```

