FROM node:latest as build-stage
WORKDIR /huetify2
COPY package*.json ./
RUN npm install
COPY ./ .
RUN npm run build

FROM nginx as production-stage
RUN mkdir /huetify2
COPY --from=build-stage /huetify2/dist /huetify2
COPY nginx.conf /etc/nginx/nginx.conf